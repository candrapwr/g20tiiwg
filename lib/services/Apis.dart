import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'dart:convert';

class Apis extends ChangeNotifier {
  var dio = Dio();
  bool loadingGlobal = false;
  bool loadingPhoto = false;
  bool loadingVideo = false;
  bool loadingRelease = false;
  bool loadingBanner = false;
  bool loadingStatistic = false;

  List<dynamic> listVideo = [];
  List<dynamic> listRelease = [];
  List<dynamic> listPhoto = [];
  List<dynamic> listBanner = [];
  Map<String, dynamic> mapStatistic = {};
  Map<String, dynamic> mapDelegate = {};

  Future getVideo({bool cb = false}) async {
    loadingVideo = true;
    var formData = FormData.fromMap(
      {
        'type': 'Videos',
        'offset': '0',
        'limit': '1000',
      },
    );
    Response response = await dio.post(
      'https://services.kemendag.go.id/g20/1.0/news',
      options: Options(headers: {
        'x-Gateway-APIKey': 'a500d9ff-8ed6-4511-b81e-0828a29a7fd2'
      }),
      data: formData,
    );

    var resp = json.decode(response.toString());
    loadingVideo = false;
    if (resp['code'] == 200) {
      listVideo = [];
      for (var value in resp['data']) {
        if (value['tag'] != null) {
          if (value['tag'].contains('TIIWG')) {
            listVideo.add(value);
          }
        }
      }
    } else {
      listVideo = [];
    }
    if (cb) {
      return listVideo;
    }
  }

  Future getRelease({int limit = 0}) async {
    loadingRelease = true;
    var formData = FormData.fromMap(
      {
        'type': 'Press Release',
        'offset': '0',
        'limit': '1000',
      },
    );
    Response response = await dio.post(
      'https://services.kemendag.go.id/g20/1.0/news',
      options: Options(headers: {
        'x-Gateway-APIKey': 'a500d9ff-8ed6-4511-b81e-0828a29a7fd2'
      }),
      data: formData,
    );

    var resp = json.decode(response.toString());
    loadingRelease = false;
    if (resp['code'] == 200) {
      listRelease = [];
      for (var value in resp['data']) {
        if (value['tag'] != null) {
          if (value['tag'].contains('TIIWG') == false) {
            if (limit == 0) {
              listRelease.add(value);
            } else {
              if (listRelease.length < limit) {
                listRelease.add(value);
              }
            }
          }
        }
      }
    } else {
      listRelease = [];
    }
  }

  Future getPhoto({bool cb = false, int limit = 0}) async {
    loadingPhoto = true;
    var formData = FormData.fromMap(
      {
        'type': 'Photo',
        'offset': '0',
        'limit': '1000',
      },
    );
    Response response = await dio.post(
      'https://services.kemendag.go.id/g20/1.0/news',
      options: Options(headers: {
        'x-Gateway-APIKey': 'a500d9ff-8ed6-4511-b81e-0828a29a7fd2'
      }),
      data: formData,
    );

    var resp = json.decode(response.toString());
    loadingPhoto = false;
    if (resp['code'] == 200) {
      listPhoto = [];
      for (var value in resp['data']) {
        if (value['tag'] != null) {
          if (value['tag'].contains('TIIWG')) {
            if (limit == 0) {
              listPhoto.add(value);
            } else {
              if (listPhoto.length < limit) {
                listPhoto.add(value);
              }
            }
          }
        }
      }
    } else {
      listPhoto = [];
    }
    if (cb) {
      return listPhoto;
    }
  }

  Future getBanner() async {
    loadingBanner = true;
    listBanner = [];

    for (var value in await getVideo(cb: true)) {
      if (listBanner.length <= 2) {
        listBanner.add(value);
      }
    }
    for (var value in await getPhoto(cb: true)) {
      if (listBanner.length <= 4) {
        listBanner.add(value);
      }
    }
    loadingBanner = false;
  }

  Future getStatistic() async {
    loadingStatistic = true;
    Response response = await dio.get(
      'https://services.kemendag.go.id/g20/1.0/summary_events',
      options: Options(headers: {
        'x-Gateway-APIKey': 'a500d9ff-8ed6-4511-b81e-0828a29a7fd2'
      }),
    );

    var resp = json.decode(response.toString());
    loadingStatistic = false;
    if (resp['code'] == 200) {
      mapStatistic = {};
      mapStatistic = resp['data'][0];
    } else {
      mapStatistic = {};
    }
    loadingStatistic = false;
  }

  Future getDelegates({String postId = ''}) async {
    loadingGlobal = true;
    var formData = FormData.fromMap(
      {
        'no_register': postId,
        'offset': '0',
        'limit': '1',
      },
    );
    Response response = await dio.post(
      'https://services.kemendag.go.id/g20/1.0/delegates',
      options: Options(headers: {
        'x-Gateway-APIKey': 'a500d9ff-8ed6-4511-b81e-0828a29a7fd2'
      }),
      data: formData,
    );

    var resp = json.decode(response.toString());
    loadingGlobal = false;
    if (resp['code'] == 200) {
      mapDelegate = {};
      mapDelegate = resp['data'][0];
    } else {
      mapDelegate = {};
    }
    loadingGlobal = false;
  }
}
