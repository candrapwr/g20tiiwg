import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:g20tiiwg/screen/splashscreen.dart';
import 'package:provider/provider.dart';
import 'package:g20tiiwg/services/apis.dart';
import 'package:nb_utils/nb_utils.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  ByteData data =
      await PlatformAssetBundle().load('assets/ca/lets-encrypt-r3.pem');
  SecurityContext.defaultContext
      .setTrustedCertificatesBytes(data.buffer.asUint8List());

  runApp(
    ChangeNotifierProvider<Apis>(create: (_) => Apis(), child: MyApp()),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'G20',
      home: SplashScreen(),
      theme: AppThemeData.g20tiiwg,
      navigatorKey: navigatorKey,
      scrollBehavior: SBehavior(),
    );
  }
}

class AppThemeData {
  AppThemeData._();
  static final ThemeData g20tiiwg = ThemeData(
    scaffoldBackgroundColor: scaffoldLightColor,
    primaryColor: Color(0xFF1157FA),
    primaryColorDark: Color(0xFF1157FA),
    errorColor: Colors.red,
    hoverColor: Colors.white54,
    dividerColor: viewLineColor,
    appBarTheme: AppBarTheme(
      color: Color(0xFFf8f8f8),
      iconTheme: IconThemeData(color: Color(0xFF2E3033)),
    ),
    textSelectionTheme: TextSelectionThemeData(cursorColor: Colors.black),
    colorScheme: ColorScheme.light(
        primary: Color(0xFF1157FA), primaryVariant: Color(0xFF1157FA)),
    cardTheme: CardTheme(color: Colors.white),
    cardColor: Colors.white,
    iconTheme: IconThemeData(color: Color(0xFF2E3033)),
    bottomSheetTheme: BottomSheetThemeData(backgroundColor: whiteColor),
    textTheme: TextTheme(
      button: TextStyle(color: Color(0xFF1157FA)),
      headline6: TextStyle(color: Color(0xFF2E3033)),
      subtitle2: TextStyle(color: Color(0xFF757575)),
    ),
    visualDensity: VisualDensity.adaptivePlatformDensity,
  ).copyWith(
    pageTransitionsTheme:
        PageTransitionsTheme(builders: <TargetPlatform, PageTransitionsBuilder>{
      TargetPlatform.android: OpenUpwardsPageTransitionsBuilder(),
      TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
      TargetPlatform.linux: OpenUpwardsPageTransitionsBuilder(),
      TargetPlatform.macOS: OpenUpwardsPageTransitionsBuilder(),
    }),
  );
}
