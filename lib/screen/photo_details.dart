import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:intl/intl.dart';

class PhotoDetailsScreen extends StatefulWidget {
  final Map<String, dynamic> dataDetail;

  PhotoDetailsScreen({required this.dataDetail});

  @override
  PhotoDetailsScreenState createState() => PhotoDetailsScreenState();
}

class PhotoDetailsScreenState extends State<PhotoDetailsScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Photo News', style: boldTextStyle(color: black, size: 18)),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              finish(context);
            }),
        elevation: 0,
        // /backgroundColor: white,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(children: [
              Text(widget.dataDetail['title_en'],
                      style: boldTextStyle(size: 20),
                      textAlign: TextAlign.center)
                  .expand(flex: 3),
            ]),
            if (widget.dataDetail['events_title'] != null) 8.height,
            if (widget.dataDetail['events_title'] != null)
              Row(children: [
                Text(
                        (widget.dataDetail['events_title'] != null)
                            ? widget.dataDetail['events_title']
                            : '',
                        style: secondaryTextStyle(),
                        textAlign: TextAlign.center)
                    .expand(flex: 3),
              ]),
            8.height,
            cacheImageWidget(
                    'https://www.g20sideevents.id/assets/upload/media/' +
                        DateFormat('yyyy')
                            .format(
                                DateTime.parse(widget.dataDetail['createdon']))
                            .toString() +
                        '/' +
                        widget.dataDetail['news_id'] +
                        '/' +
                        widget.dataDetail['filename'],
                    200,
                    width: context.width(),
                    fit: BoxFit.cover)
                .cornerRadiusWithClipRRect(0),
            8.height,
            ListTile(
              contentPadding: EdgeInsets.all(0),
              title: Text(
                widget.dataDetail['minister_en'],
                style: boldTextStyle(),
              ),
              subtitle: Text(
                  DateFormat('EEEE, d MMMM yyyy')
                      .format(DateTime.parse(widget.dataDetail['createdon']))
                      .toString(),
                  style: secondaryTextStyle()),
            ),
            8.height,
            Text(widget.dataDetail['desc_en'],
                style: primaryTextStyle(), textAlign: TextAlign.justify),
            32.height,
          ],
        ).paddingOnly(left: 16, right: 16),
      ),
    );
  }

  Widget cacheImageWidget(String? url, double height,
      {double? width, BoxFit? fit}) {
    if (url.validate().startsWith('http')) {
      if (isMobile) {
        return CachedNetworkImage(
          imageUrl: '$url',
          height: height,
          width: width,
          fit: fit ?? BoxFit.cover,
          errorWidget: (_, __, ___) {
            return SizedBox(height: height, width: width);
          },
        );
      } else {
        return Image.network(url!,
            height: height, width: width, fit: fit ?? BoxFit.cover);
      }
    } else {
      return Image.asset(url!,
          height: height, width: width, fit: fit ?? BoxFit.cover);
    }
  }
}
