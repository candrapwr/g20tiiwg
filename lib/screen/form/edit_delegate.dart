import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:intl/intl.dart';
import 'package:search_choices/search_choices.dart';

class EditDelegateScreen extends StatefulWidget {
  final Map<String, dynamic> dataIn;

  EditDelegateScreen({required this.dataIn});

  @override
  EditDelegateScreenState createState() => EditDelegateScreenState();
}

class EditDelegateScreenState extends State<EditDelegateScreen> {
  final formKey = GlobalKey<FormState>();

  static final List<dynamic> flightTypeMap = [
    "Inbound Flight",
    "Indonesia Domestic Flight",
    "Outbound Flight",
  ];
  String flightType = '';
  TextEditingController flightNumber = TextEditingController();
  TextEditingController flightFrom = TextEditingController();
  TextEditingController flightFromDate = TextEditingController();
  TextEditingController flightTo = TextEditingController();
  TextEditingController flightToDate = TextEditingController();

  @override
  void initState() {
    super.initState();
    setState(() {
      flightNumber.text = widget.dataIn['flight_number'];
      flightFrom.text = widget.dataIn['from'];
      flightFromDate.text = DateFormat('yyyy-MM-dd')
          .format(DateTime.parse(widget.dataIn['departure_time']));
      flightTo.text = widget.dataIn['to'];
      flightToDate.text = DateFormat('yyyy-MM-dd')
          .format(DateTime.parse(widget.dataIn['arrival_time']));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit', style: boldTextStyle(color: black, size: 18)),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              finish(context);
            }),
        elevation: 0,
      ),
      body: SingleChildScrollView(
          child: Container(
              child: Form(
                  key: formKey,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(
                              left: 15.0, right: 15.0, bottom: 0, top: 25.0),
                          child: SearchChoices.single(
                            items: flightTypeMap
                                .map<DropdownMenuItem<String>>(
                                    (value) => DropdownMenuItem<String>(
                                          child: Text(value),
                                          value: value,
                                        ))
                                .toList(),
                            value: widget.dataIn['type'],
                            hint: "Choose type",
                            searchHint: "Choose type",
                            onChanged: (value) {
                              setState(() {
                                flightType = value;
                              });
                            },
                            label: 'Choose type ',
                            style: TextStyle(color: Colors.black),
                            underline: Container(
                              height: 1.0,
                              decoration: BoxDecoration(
                                border:
                                    Border.all(color: Colors.grey, width: 1.0),
                              ),
                            ),
                            isExpanded: true,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Choose type';
                              }

                              return null;
                            },
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 15.0, right: 15.0, bottom: 0, top: 15.0),
                          child: TextFormField(
                            key: Key('flightNumber'),
                            controller: flightNumber,
                            decoration: InputDecoration(
                              hintText: 'Flight Number',
                              labelText: 'Flight Number',
                              prefixIcon: Icon(Icons.confirmation_number),
                            ),
                            style:
                                TextStyle(fontSize: 16.0, color: Colors.black),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'isEmpty';
                              }
                              return null;
                            },
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 15.0, right: 15.0, bottom: 0, top: 15.0),
                          child: TextFormField(
                            key: Key('flightFrom'),
                            controller: flightFrom,
                            decoration: InputDecoration(
                              hintText: 'From',
                              labelText: 'From',
                              prefixIcon: Icon(Icons.location_city),
                            ),
                            style:
                                TextStyle(fontSize: 16.0, color: Colors.black),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'isEmpty';
                              }
                              return null;
                            },
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 15.0, right: 15.0, bottom: 0, top: 15.0),
                          child: TextFormField(
                            enableInteractiveSelection: false,
                            key: Key('flightFromDate'),
                            controller: flightFromDate,
                            decoration: InputDecoration(
                              hintText: 'From Date',
                              labelText: 'From Date',
                              prefixIcon: Icon(Icons.date_range),
                            ),
                            style:
                                TextStyle(fontSize: 16.0, color: Colors.black),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'isEmpty';
                              }
                              return null;
                            },
                            onTap: () async {
                              DateTime date = DateTime(2010);
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                              date = (await showDatePicker(
                                  context: context,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime(2010),
                                  lastDate: DateTime(2100)))!;
                              flightFromDate.text =
                                  DateFormat('yyyy-MM-dd').format(date);
                            },
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 15.0, right: 15.0, bottom: 0, top: 15.0),
                          child: TextFormField(
                            key: Key('flightTo'),
                            controller: flightTo,
                            decoration: InputDecoration(
                              hintText: 'To',
                              labelText: 'To',
                              prefixIcon: Icon(Icons.location_city),
                            ),
                            style:
                                TextStyle(fontSize: 16.0, color: Colors.black),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'isEmpty';
                              }
                              return null;
                            },
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 15.0, right: 15.0, bottom: 0, top: 15.0),
                          child: TextFormField(
                              enableInteractiveSelection: false,
                              key: Key('flightToDate'),
                              controller: flightToDate,
                              decoration: InputDecoration(
                                hintText: 'To Date',
                                labelText: 'To Date',
                                prefixIcon: Icon(Icons.date_range),
                              ),
                              style: TextStyle(
                                  fontSize: 16.0, color: Colors.black),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'isEmpty';
                                }
                                return null;
                              },
                              onTap: () async {
                                DateTime date = DateTime(2010);
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                                date = (await showDatePicker(
                                    context: context,
                                    initialDate: DateTime.now(),
                                    firstDate: DateTime(2010),
                                    lastDate: DateTime(2100)))!;
                                flightToDate.text =
                                    DateFormat('yyyy-MM-dd').format(date);
                              }),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 15.0, right: 15.0, bottom: 15.0, top: 15),
                          child: ElevatedButton.icon(
                            style: ElevatedButton.styleFrom(
                                primary: Color(0xff012233),
                                onPrimary: Colors.white,
                                minimumSize: Size(double.infinity, 40)),
                            onPressed: () {},
                            icon: Icon(
                              Icons.save,
                              size: 16,
                            ),
                            label: const Text('Save'),
                          ),
                        )
                      ])))),
    );
  }
}
