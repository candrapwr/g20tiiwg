import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:intl/intl.dart';

class VideoDetailsScreen extends StatefulWidget {
  final Map<String, dynamic> dataDetail;

  VideoDetailsScreen({required this.dataDetail});

  @override
  VideoDetailsScreenState createState() => VideoDetailsScreenState();
}

class VideoDetailsScreenState extends State<VideoDetailsScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
            Text('Video Detail', style: boldTextStyle(color: black, size: 18)),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              finish(context);
            }),
        elevation: 0,
        //backgroundColor: white,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: YoutubePlayer(
                controller: YoutubePlayerController(
                  initialVideoId: YoutubePlayer.convertUrlToId(
                          widget.dataDetail['filename'])
                      .toString(),
                  flags: YoutubePlayerFlags(
                    hideControls: false,
                    controlsVisibleAtStart: true,
                    autoPlay: false,
                    mute: false,
                  ),
                ),
                showVideoProgressIndicator: true,
              ),
            ).paddingOnly(left: 5, right: 5),
            16.height,
            Row(
              children: [
                Text(widget.dataDetail['title_en'],
                        style: boldTextStyle(size: 20),
                        textAlign: TextAlign.left)
                    .expand(flex: 3),
              ],
            ).paddingOnly(left: 16, right: 16),
            8.height,
            Row(
              children: [
                Flexible(
                    child: Text(widget.dataDetail['minister_en'],
                        style: boldTextStyle())),
              ],
            ).paddingOnly(left: 16, right: 16),
            8.height,
            Row(
              children: [
                Text(
                    DateFormat('EEEE, d MMMM yyyy')
                        .format(DateTime.parse(widget.dataDetail['createdon']))
                        .toString(),
                    style: secondaryTextStyle()),
              ],
            ).paddingOnly(left: 16, right: 16),
            16.height,
            Text(widget.dataDetail['desc_en'],
                    style: primaryTextStyle(), textAlign: TextAlign.justify)
                .paddingOnly(left: 16, right: 16),
            32.height,
          ],
        ),
      ),
    );
  }

  Widget cacheImageWidget(String? url, double height,
      {double? width, BoxFit? fit}) {
    if (url.validate().startsWith('http')) {
      if (isMobile) {
        return CachedNetworkImage(
          imageUrl: '$url',
          height: height,
          width: width,
          fit: fit ?? BoxFit.cover,
          errorWidget: (_, __, ___) {
            return SizedBox(height: height, width: width);
          },
        );
      } else {
        return Image.network(url!,
            height: height, width: width, fit: fit ?? BoxFit.cover);
      }
    } else {
      return Image.asset(url!,
          height: height, width: width, fit: fit ?? BoxFit.cover);
    }
  }
}
