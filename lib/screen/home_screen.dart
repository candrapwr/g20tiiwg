import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:g20tiiwg/component/comp_home.dart';
import 'package:g20tiiwg/component/comp_photo.dart';
import 'package:g20tiiwg/component/comp_video.dart';
import 'package:g20tiiwg/component/comp_release.dart';
import 'package:g20tiiwg/screen/delegates_details.dart';
import 'package:g20tiiwg/screen/about_screen.dart';
import 'package:flutter_svg/svg.dart';

class HomeScreen extends StatefulWidget {
  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  var isSelected;
  TabController? tabController;
  var txt = TextEditingController();
  bool _validate = false;

  @override
  void initState() {
    super.initState();
    init();
  }

  Future<void> init() async {
    tabController = TabController(length: 4, vsync: this);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    tabController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('G20 TIIWG', style: boldTextStyle(color: black, size: 20)),
        backgroundColor: white,
        centerTitle: true,
        bottom: TabBar(
          controller: tabController,
          tabs: [
            Tab(text: 'Home'),
            Tab(text: 'Photo News'),
            Tab(text: 'Press Release'),
            Tab(text: 'Videos')
          ],
          labelStyle: boldTextStyle(),
          labelColor: black,
          unselectedLabelStyle: primaryTextStyle(),
          unselectedLabelColor: grey,
          isScrollable: true,
          indicatorColor: Color(0xFFFD5530),
          indicatorWeight: 3,
          indicatorSize: TabBarIndicatorSize.tab,
        ),
      ),
      bottomNavigationBar: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(16),
            height: 70,
            decoration: BoxDecoration(
              color: context.scaffoldBackgroundColor,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              boxShadow: [
                BoxShadow(
                  color: shadowColorGlobal,
                  blurRadius: 10,
                  spreadRadius: 2,
                ),
              ],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Flexible(
                  child: tabItem(1, "assets/images/globe.svg", "About G20"),
                  flex: 2,
                ),
                Flexible(
                  child: tabItem(2, "assets/images/info.svg", "About TIIWG"),
                  flex: 2,
                ),
                Flexible(
                  child: tabItem(3, "assets/images/regis.svg", "Check Reg"),
                  flex: 2,
                ),
              ],
            ),
          ),
        ],
      ),
      body: TabBarView(
        controller: tabController,
        children: [
          HomeComponent(),
          PhotoComponent(),
          ReleaseComponent(),
          VideoComponent(),
        ],
      ),
    );
  }

  Widget tabItem(var pos, var icon, var name) {
    return GestureDetector(
      onTap: () {
        if (pos == 3) {
          getIdDelegate(context);
        }
        if (pos == 1) {
          AboutScreen(
                  inputUrl: 'https://www.g20sideevents.id/tiiwg/about/g20',
                  inputTitle: 'About G20')
              .launch(context);
        }
        if (pos == 2) {
          AboutScreen(
                  inputUrl: 'https://g20sideevents.id/tiiwg/apps/about',
                  inputTitle: 'About TIIWG')
              .launch(context);
        }
        setState(() {
          isSelected = pos;
        });
      },
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Container(
          alignment: Alignment.center,
          decoration: isSelected == pos
              ? BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: Color(0XFF313384),
                  borderRadius: BorderRadius.circular(8))
              : BoxDecoration(),
          child: Padding(
            padding: EdgeInsets.all(6.0),
            child: Column(
              children: <Widget>[
                SvgPicture.asset(
                  icon,
                  width: 20,
                  height: 20,
                  color:
                      isSelected == pos ? Color(0XFFffffff) : Color(0XFF747474),
                ),
                text(name,
                    textColor: isSelected == pos
                        ? Color(0XFFffffff)
                        : Color(0XFF747474),
                    fontSize: 12.0)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget text(
    String? text, {
    var fontSize = 18.0,
    Color? textColor,
    var fontFamily,
    var isCentered = false,
    var maxLine = 1,
    var latterSpacing = 0.5,
    bool textAllCaps = false,
    var isLongText = false,
    bool lineThrough = false,
  }) {
    return Text(
      textAllCaps ? text!.toUpperCase() : text!,
      textAlign: isCentered ? TextAlign.center : TextAlign.start,
      maxLines: isLongText ? null : maxLine,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
        fontFamily: fontFamily ?? null,
        fontSize: fontSize,
        color: textColor ?? Colors.white54,
        height: 1.5,
        letterSpacing: latterSpacing,
        decoration:
            lineThrough ? TextDecoration.lineThrough : TextDecoration.none,
      ),
    );
  }

  Future<void> getIdDelegate(BuildContext context) async {
    var delegateId = '';
    showGeneralDialog(
      context: context,
      pageBuilder: (ctx, a1, a2) {
        return Container();
      },
      transitionBuilder: (ctx, a1, a2, child) {
        var curve = Curves.easeInOut.transform(a1.value);
        return Transform.scale(
          scale: curve,
          child: AlertDialog(
            title: Text('Enter your Delegates ID'),
            content: TextField(
              onChanged: (value) {
                setState(() {
                  delegateId = value.toUpperCase();
                });
              },
              controller: txt,
              decoration: InputDecoration(
                hintText: "Delegates ID",
                errorText: _validate ? 'Delegate ID Can\'t Be Empty' : null,
              ),
            ),
            actions: <Widget>[
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.red,
                  onPrimary: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  elevation: 15.0,
                ),
                child: Text('Cencel'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.blueGrey,
                  onPrimary: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  elevation: 15.0,
                ),
                child: Text('Check Registration'),
                onPressed: () {
                  setState(() {
                    if (txt.text.isEmpty) {
                      _validate = true;
                    } else {
                      _validate = false;
                      Navigator.pop(context);
                      txt.text = '';
                      DelegatesDetailsScreen(inputId: delegateId)
                          .launch(context);
                      delegateId = '';
                    }
                  });
                },
              ),
            ],
          ),
        );
      },
      transitionDuration: const Duration(milliseconds: 500),
    );
  }
}
