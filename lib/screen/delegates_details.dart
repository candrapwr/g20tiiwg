import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:g20tiiwg/services/apis.dart';
import 'package:g20tiiwg/screen/form/edit_delegate.dart';

class DelegatesDetailsScreen extends StatefulWidget {
  final String inputId;

  DelegatesDetailsScreen({required this.inputId});

  @override
  DelegatesDetailsScreenState createState() => DelegatesDetailsScreenState();
}

class DelegatesDetailsScreenState extends State<DelegatesDetailsScreen>
    with SingleTickerProviderStateMixin {
  TabController? tabController;

  @override
  void initState() {
    super.initState();
    final provStage = Provider.of<Apis>(context, listen: false);
    provStage.getDelegates(postId: widget.inputId).whenComplete(() => {
          if (provStage.mapDelegate['createdon'] == null)
            {
              init(),
              Navigator.of(context).pop(),
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertNotFound();
                  })
            }
          else
            {
              init(
                  numTab:
                      (provStage.mapDelegate['peserta_in_person'].length == 0)
                          ? 3
                          : 4)
            },
          setState(() {})
        });
  }

  Future<void> init({int numTab = 0}) async {
    tabController = TabController(length: numTab, vsync: this, initialIndex: 0);
  }

  @override
  void dispose() {
    tabController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final provApis = Provider.of<Apis>(context, listen: true);
    return Scaffold(
      appBar: AppBar(
        title: Text('Delegates Information',
            style: boldTextStyle(color: black, size: 18)),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              finish(context);
            }),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: (provApis.loadingGlobal == false)
          ? (provApis.mapDelegate['createdon'] != null)
              ? ListView(
                  children: <Widget>[
                    Container(
                      height: 250,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("assets/images/bg.jpg"),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              CircleAvatar(
                                backgroundColor: Colors.white70,
                                minRadius: 53.0,
                                child: CircleAvatar(
                                  radius: 50.0,
                                  backgroundImage: NetworkImage(
                                      'https://www.g20sideevents.id/assets/upload/peserta/' +
                                          DateFormat('yyyy')
                                              .format(DateTime.parse(provApis
                                                  .mapDelegate['createdon']))
                                              .toString() +
                                          '/' +
                                          provApis.mapDelegate['photo']),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            provApis.mapDelegate['first_name'] +
                                ' ' +
                                provApis.mapDelegate['last_name'],
                            style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                          Text(
                            provApis.mapDelegate['email'],
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              color: Color(0xff1c1b5c),
                              height: 5,
                              child: ListTile(
                                title: Text(''),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              color: Color(0xff1c1b5c),
                              height: 5,
                              child: ListTile(
                                title: Text(''),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    (provApis.mapDelegate['peserta_in_person'].length != 0)
                        ? wgItinerary(provApis: provApis)
                        : wgNotItinerary(provApis: provApis),
                  ],
                )
              : Center(
                  child: Text('-----'),
                )
          : Center(
              child: CircularProgressIndicator(),
            ),
    );
  }

  Widget wgPersonalInfo({provApis}) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text(
            'Attendance Type',
            style: TextStyle(
              color: Color(0xff012233),
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(
            provApis.mapDelegate['attendance_type'],
            style: TextStyle(fontSize: 14),
          ),
        ),
        ListTile(
          title: Text(
            'Delegate Status',
            style: TextStyle(
              color: Color(0xff012233),
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(
            provApis.mapDelegate['position'],
            style: TextStyle(fontSize: 14),
          ),
        ),
        ListTile(
          title: Text(
            'Ministry/Organization',
            style: TextStyle(
              color: Color(0xff012233),
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(
            provApis.mapDelegate['organization'],
            style: TextStyle(fontSize: 14),
          ),
        ),
        ListTile(
          title: Text(
            'Position Title',
            style: TextStyle(
              color: Color(0xff012233),
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(
            provApis.mapDelegate['position_title'],
            style: TextStyle(fontSize: 14),
          ),
        ),
        ListTile(
          title: Text(
            'Country',
            style: TextStyle(
              color: Color(0xff012233),
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(
            provApis.mapDelegate['country_name'],
            style: TextStyle(fontSize: 14),
          ),
        ),
        ListTile(
          title: Text(
            'Attendance',
            style: TextStyle(
              color: Color(0xff012233),
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(
            provApis.mapDelegate['attendance'],
            style: TextStyle(fontSize: 14),
          ),
        ),
        ListTile(
          title: Text(
            'Phone Number',
            style: TextStyle(
              color: Color(0xff012233),
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(
            provApis.mapDelegate['phone'],
            style: TextStyle(fontSize: 14),
          ),
        ),
        ListTile(
          title: Text(
            'Batik Size',
            style: TextStyle(
              color: Color(0xff012233),
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(
            provApis.mapDelegate['size_batik'],
            style: TextStyle(fontSize: 14),
          ),
        ),
        Divider(),
      ],
    );
  }

  Widget wgItinerary({provApis}) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
            child: TabBar(
              controller: tabController,
              labelStyle: boldTextStyle(),
              labelColor: black,
              unselectedLabelStyle: primaryTextStyle(),
              unselectedLabelColor: grey,
              isScrollable: true,
              indicatorColor: Color(0xFFFD5530),
              indicatorWeight: 3,
              indicatorSize: TabBarIndicatorSize.tab,
              tabs: <Widget>[
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    //Icon(Icons.person_pin_circle_outlined),
                    Text(' Personal Information'.toUpperCase()),
                  ],
                ),
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    //Icon(Icons.flight),
                    Text(' Flight'.toUpperCase()),
                  ],
                ),
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    //Icon(Icons.hotel),
                    Text(' Accomodation & Insurance'.toUpperCase()),
                  ],
                ),
                Text('Passport'.toUpperCase()),
              ],
            ),
          ),
          Container(
              height: 600,
              child: TabBarView(
                physics: NeverScrollableScrollPhysics(),
                controller: tabController,
                children: [
                  wgPersonalInfo(provApis: provApis),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(0.0),
                        child: ListView.builder(
                            scrollDirection: Axis.vertical,
                            itemCount: provApis
                                .mapDelegate['peserta_in_person'][0]
                                    ['peserta_flight']
                                .length,
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            itemBuilder: (context, index) {
                              return Container(
                                margin: EdgeInsets.only(bottom: 5),
                                child: Column(
                                  children: <Widget>[
                                    ListTile(
                                      title: Text(
                                        provApis.mapDelegate[
                                                'peserta_in_person'][0]
                                            ['peserta_flight'][index]['type'],
                                        style: TextStyle(
                                          color: Color(0xff012233),
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      subtitle: Text(
                                        'Flight Number ' +
                                            provApis.mapDelegate['peserta_in_person']
                                                    [0]['peserta_flight'][index]
                                                ['flight_number'] +
                                            '\nFrom : ' +
                                            provApis.mapDelegate['peserta_in_person']
                                                    [0]['peserta_flight'][index]
                                                ['from'] +
                                            ' at ' +
                                            DateFormat('d MMMM yyyy')
                                                .format(DateTime.parse(
                                                    provApis.mapDelegate['peserta_in_person'][0]
                                                            ['peserta_flight'][index]
                                                        ['departure_time']))
                                                .toString() +
                                            '\nTo: ' +
                                            provApis.mapDelegate['peserta_in_person'][0]
                                                ['peserta_flight'][index]['to'] +
                                            ' at ' +
                                            DateFormat('d MMMM yyyy').format(DateTime.parse(provApis.mapDelegate['peserta_in_person'][0]['peserta_flight'][index]['arrival_time'])).toString(),
                                        style: TextStyle(fontSize: 14),
                                      ),
                                      leading: Icon(
                                        (provApis.mapDelegate[
                                                            'peserta_in_person']
                                                        [0]['peserta_flight']
                                                    [index]['type'] ==
                                                'Outbound Flight')
                                            ? Icons.flight_takeoff
                                            : Icons.flight_land,
                                        color: Colors.redAccent,
                                      ),
                                      trailing: Column(
                                        children: <Widget>[
                                          Container(
                                            child: IconButton(
                                              icon: Icon(
                                                  Icons.edit_road_outlined,
                                                  color: Colors.green),
                                              onPressed: () {
                                                EditDelegateScreen(
                                                        dataIn: provApis
                                                                    .mapDelegate[
                                                                'peserta_in_person'][0]
                                                            [
                                                            'peserta_flight'][index])
                                                    .launch(context);
                                              },
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    Divider(
                                            height: 0.5,
                                            color: Color(0xFFDADADA),
                                            thickness: 1)
                                        .paddingOnly(left: 16, right: 16)
                                  ],
                                ),
                              );
                            }),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(0.0),
                        child: ListView.builder(
                            scrollDirection: Axis.vertical,
                            itemCount: provApis
                                .mapDelegate['peserta_in_person'].length,
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            itemBuilder: (context, index) {
                              return Container(
                                margin: EdgeInsets.only(bottom: 5),
                                child: Column(
                                  children: <Widget>[
                                    ListTile(
                                      title: Text(
                                        provApis.mapDelegate[
                                                'peserta_in_person'][index]
                                            ['hotel'],
                                        style: TextStyle(
                                          color: Color(0xff012233),
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      subtitle: Text(
                                        provApis.mapDelegate[
                                                'peserta_in_person'][index]
                                            ['location'],
                                        style: TextStyle(fontSize: 14),
                                      ),
                                      leading: Icon(
                                        Icons.hotel,
                                        color: Colors.redAccent,
                                      ),
                                      trailing: Column(
                                        children: <Widget>[
                                          Container(
                                            child: IconButton(
                                              icon: Icon(
                                                  Icons.edit_road_outlined,
                                                  color: Colors.green),
                                              onPressed: () {
                                                EditDelegateScreen(
                                                        dataIn: provApis
                                                                    .mapDelegate[
                                                                'peserta_in_person']
                                                            [index])
                                                    .launch(context);
                                              },
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    Divider(
                                            height: 0.5,
                                            color: Color(0xFFDADADA),
                                            thickness: 1)
                                        .paddingOnly(left: 16, right: 16)
                                  ],
                                ),
                              );
                            }),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(0.0),
                        child: ListView.builder(
                            scrollDirection: Axis.vertical,
                            itemCount: 1,
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            itemBuilder: (context, index) {
                              return Container(
                                margin: EdgeInsets.only(bottom: 5),
                                child: Column(
                                  children: <Widget>[
                                    ListTile(
                                      title: Text(
                                        'Passport Type',
                                        style: TextStyle(
                                          color: Color(0xff012233),
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      subtitle: Text(
                                        provApis
                                            .mapDelegate['peserta_in_person'][0]
                                                ['peserta_passport'][0]
                                                ['passport_type']
                                            .toString(),
                                        style: TextStyle(fontSize: 14),
                                      ),
                                    ),
                                    ListTile(
                                      title: Text(
                                        'Number',
                                        style: TextStyle(
                                          color: Color(0xff012233),
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      subtitle: Text(
                                        provApis
                                            .mapDelegate['peserta_in_person'][0]
                                                ['peserta_passport'][0]
                                                ['passport_number']
                                            .toString(),
                                        style: TextStyle(fontSize: 14),
                                      ),
                                    ),
                                    ListTile(
                                      title: Text(
                                        'Expired Date',
                                        style: TextStyle(
                                          color: Color(0xff012233),
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      subtitle: Text(
                                        provApis
                                            .mapDelegate['peserta_in_person'][0]
                                                ['peserta_passport'][0]
                                                ['expiring_date']
                                            .toString(),
                                        style: TextStyle(fontSize: 14),
                                      ),
                                    ),
                                    ListTile(
                                      title: Text(
                                        'Issuing Country',
                                        style: TextStyle(
                                          color: Color(0xff012233),
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      subtitle: Text(
                                        provApis
                                            .mapDelegate['peserta_in_person'][0]
                                                ['peserta_passport'][0]
                                                ['citizenship']
                                            .toString(),
                                        style: TextStyle(fontSize: 14),
                                      ),
                                    ),
                                    ListTile(
                                      title: Text(
                                        'Citizenship',
                                        style: TextStyle(
                                          color: Color(0xff012233),
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      subtitle: Text(
                                        provApis
                                            .mapDelegate['peserta_in_person'][0]
                                                ['peserta_passport'][0]
                                                ['citizenship']
                                            .toString(),
                                        style: TextStyle(fontSize: 14),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }),
                      ),
                    ],
                  ),
                ],
              )),
        ],
      ),
    );
  }

  Widget wgNotItinerary({provApis}) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
            child: TabBar(
              controller: tabController,
              labelStyle: boldTextStyle(),
              labelColor: black,
              unselectedLabelStyle: primaryTextStyle(),
              unselectedLabelColor: grey,
              isScrollable: true,
              indicatorColor: Color(0xFFFD5530),
              indicatorWeight: 3,
              indicatorSize: TabBarIndicatorSize.tab,
              tabs: <Widget>[
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    //Icon(Icons.person_pin_circle_outlined),
                    Text(' Personal Information'.toUpperCase()),
                  ],
                ),
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    //Icon(Icons.flight),
                    Text('D.A.O'.toUpperCase()),
                  ],
                ),
                Text('IT Contact Point'.toUpperCase()),
              ],
            ),
          ),
          Container(
              height: 600,
              child: TabBarView(
                physics: NeverScrollableScrollPhysics(),
                controller: tabController,
                children: [
                  wgPersonalInfo(provApis: provApis),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(0.0),
                        child: ListView.builder(
                            scrollDirection: Axis.vertical,
                            itemCount:
                                provApis.mapDelegate['peserta_dao'].length,
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            itemBuilder: (context, index) {
                              return Container(
                                margin: EdgeInsets.only(bottom: 5),
                                child: Column(
                                  children: <Widget>[
                                    ListTile(
                                      title: Text(
                                        provApis.mapDelegate['peserta_dao']
                                                [index]['title'] +
                                            '. ' +
                                            provApis.mapDelegate['peserta_dao']
                                                [index]['first_name'] +
                                            ' ' +
                                            provApis.mapDelegate['peserta_dao']
                                                [index]['last_name'],
                                        style: TextStyle(
                                          color: Color(0xff012233),
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      subtitle: Text(
                                        provApis.mapDelegate['peserta_dao']
                                                [index]['email'] +
                                            '\n' +
                                            provApis.mapDelegate['peserta_dao']
                                                [index]['phone'],
                                        style: TextStyle(fontSize: 14),
                                      ),
                                      leading: Icon(
                                        Icons.people_alt,
                                        color: Colors.redAccent,
                                      ),
                                      trailing: Icon(Icons.edit_road_outlined,
                                          color: Colors.green),
                                    ),
                                    Divider(
                                            height: 0.5,
                                            color: Color(0xFFDADADA),
                                            thickness: 1)
                                        .paddingOnly(left: 16, right: 16)
                                  ],
                                ),
                              );
                            }),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(0.0),
                        child: ListView.builder(
                            scrollDirection: Axis.vertical,
                            itemCount:
                                provApis.mapDelegate['peserta_dao'].length,
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            itemBuilder: (context, index) {
                              return Container(
                                margin: EdgeInsets.only(bottom: 5),
                                child: Column(
                                  children: <Widget>[
                                    ListTile(
                                      title: Text(
                                        provApis.mapDelegate['peserta_virtual']
                                                [index]['title'] +
                                            '. ' +
                                            provApis.mapDelegate[
                                                    'peserta_virtual'][index]
                                                ['first_name'] +
                                            ' ' +
                                            provApis.mapDelegate[
                                                    'peserta_virtual'][index]
                                                ['last_name'],
                                        style: TextStyle(
                                          color: Color(0xff012233),
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      subtitle: Text(
                                        provApis.mapDelegate['peserta_virtual']
                                                [index]['email'] +
                                            '\n' +
                                            provApis.mapDelegate[
                                                    'peserta_virtual'][index]
                                                ['phone'],
                                        style: TextStyle(fontSize: 14),
                                      ),
                                      leading: Icon(
                                        Icons.people_alt,
                                        color: Colors.redAccent,
                                      ),
                                      trailing: Icon(Icons.edit_road_outlined,
                                          color: Colors.green),
                                    ),
                                    Divider(
                                            height: 0.5,
                                            color: Color(0xFFDADADA),
                                            thickness: 1)
                                        .paddingOnly(left: 16, right: 16)
                                  ],
                                ),
                              );
                            }),
                      ),
                    ],
                  ),
                ],
              )),
        ],
      ),
    );
  }
}

class AlertNotFound extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        child: Stack(
          clipBehavior: Clip.none,
          alignment: Alignment.topCenter,
          children: [
            Container(
              height: 210,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 60, 10, 10),
                child: Column(
                  children: [
                    Text(
                      'Warning !!!',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Delegates ID not found',
                      style: TextStyle(fontSize: 18),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.redAccent),
                      ),
                      child: Text(
                        'OKAY',
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Positioned(
                top: -40,
                child: CircleAvatar(
                  backgroundColor: Colors.redAccent,
                  radius: 40,
                  child: Icon(
                    Icons.warning,
                    color: Colors.white,
                    size: 40,
                  ),
                )),
          ],
        ));
  }
}
