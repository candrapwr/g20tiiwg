import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:advance_pdf_viewer_fork/advance_pdf_viewer_fork.dart';
import 'package:intl/intl.dart';

class ReleaseDetailsScreen extends StatefulWidget {
  final Map<String, dynamic> dataDetail;

  ReleaseDetailsScreen({required this.dataDetail});

  @override
  ReleaseDetailsScreenState createState() => ReleaseDetailsScreenState();
}

class ReleaseDetailsScreenState extends State<ReleaseDetailsScreen> {
  bool _isLoading = true;
  late PDFDocument document;

  @override
  void initState() {
    super.initState();
    loadDocument();
  }

  loadDocument() async {
    document = await PDFDocument.fromURL(
        'https://www.g20sideevents.id/assets/upload/press_release/' +
            DateFormat('yyyy')
                .format(DateTime.parse(widget.dataDetail['createdon']))
                .toString() +
            '/' +
            widget.dataDetail['filename']);
    setState(() => _isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    //loadDocument();
    return Scaffold(
      appBar: AppBar(
        title:
            Text('Press Release', style: boldTextStyle(color: black, size: 18)),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              finish(context);
            }),
        elevation: 0,
        // /backgroundColor: white,
      ),
      body: Center(
        child: _isLoading
            ? Center(child: CircularProgressIndicator())
            : PDFViewer(
                document: document,
                zoomSteps: 1,
              ),
      ),
    );
  }
}
