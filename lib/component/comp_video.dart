import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:g20tiiwg/screen/video_details.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:g20tiiwg/services/apis.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class VideoComponent extends StatefulWidget {
  @override
  VideoComponentState createState() => VideoComponentState();
}

class VideoComponentState extends State<VideoComponent> {
  @override
  void initState() {
    super.initState();
    Provider.of<Apis>(context, listen: false)
        .getVideo()
        .whenComplete(() => {setState(() {})});
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    final provApis = Provider.of<Apis>(context, listen: true);
    return (provApis.loadingVideo == false)
        ? ListView.separated(
            shrinkWrap: true,
            physics: ScrollPhysics(),
            itemCount: provApis.listVideo.length,
            padding: EdgeInsets.all(16),
            itemBuilder: (context, index) {
              return Container(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Stack(
                      alignment: AlignmentDirectional.bottomCenter,
                      children: [
                        Image.network(
                          'https://i.ytimg.com/vi/' +
                              YoutubePlayer.convertUrlToId(
                                      provApis.listVideo[index]['filename'])
                                  .toString() +
                              '/maxresdefault.jpg',
                          height: 100,
                          fit: BoxFit.fill,
                          loadingBuilder: (BuildContext context, Widget child,
                              ImageChunkEvent? loadingProgress) {
                            if (loadingProgress == null) return child;
                            return Center(
                              child: CircularProgressIndicator(
                                value: loadingProgress.expectedTotalBytes !=
                                        null
                                    ? loadingProgress.cumulativeBytesLoaded /
                                        loadingProgress.expectedTotalBytes!
                                    : null,
                              ),
                            );
                          },
                        ).cornerRadiusWithClipRRect(16),
                        Container(
                          padding: EdgeInsets.only(left: 8, right: 8),
                          decoration: boxDecorationWithRoundedCorners(
                              backgroundColor: black.withOpacity(0.2)),
                          child: Text('20:20',
                              style: primaryTextStyle(color: white, size: 14)),
                        ),
                      ],
                    ).expand(flex: 1),
                    8.width,
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Videos',
                            style: boldTextStyle(color: Color(0xFFFD5530))),
                        Text(provApis.listVideo[index]['title_en'],
                            style: boldTextStyle(),
                            softWrap: true,
                            maxLines: 3),
                        8.height,
                        Text(
                            DateFormat('EEEE, d MMMM yyyy')
                                .format(DateTime.parse(
                                    provApis.listVideo[index]['createdon']))
                                .toString(),
                            style: secondaryTextStyle()),
                      ],
                    ).expand(flex: 2),
                  ],
                ),
              ).onTap(() {
                VideoDetailsScreen(dataDetail: provApis.listVideo[index])
                    .launch(context);
              });
            },
            separatorBuilder: (context, index) {
              return Divider();
            },
          )
        : Center(
            child: CircularProgressIndicator(),
          );
  }
}
