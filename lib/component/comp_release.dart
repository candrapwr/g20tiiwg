import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:g20tiiwg/screen/release_details.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:g20tiiwg/services/apis.dart';

class ReleaseComponent extends StatefulWidget {
  @override
  ReleaseComponentState createState() => ReleaseComponentState();
}

class ReleaseComponentState extends State<ReleaseComponent> {
  @override
  void initState() {
    super.initState();
    Provider.of<Apis>(context, listen: false)
        .getRelease()
        .whenComplete(() => {setState(() {})});
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    final provApis = Provider.of<Apis>(context, listen: true);
    return (provApis.loadingRelease == false)
        ? ListView.separated(
            shrinkWrap: true,
            physics: ScrollPhysics(),
            itemCount: provApis.listRelease.length,
            padding: EdgeInsets.all(16),
            itemBuilder: (context, index) {
              return Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Press Release',
                          style: boldTextStyle(color: Color(0xFFFD5530))),
                      Text(provApis.listRelease[index]['title_en'],
                          style: boldTextStyle(), softWrap: true, maxLines: 3),
                      8.height,
                      Text(
                          DateFormat('EEEE, d MMMM yyyy')
                              .format(DateTime.parse(
                                  provApis.listRelease[index]['createdon']))
                              .toString(),
                          style: secondaryTextStyle()),
                    ],
                  ).expand(flex: 2),
                ],
              ).onTap(() {
                ReleaseDetailsScreen(dataDetail: provApis.listRelease[index])
                    .launch(context);
              });
            },
            separatorBuilder: (context, index) {
              return Divider();
            },
          )
        : Center(
            child: CircularProgressIndicator(),
          );
  }
}
