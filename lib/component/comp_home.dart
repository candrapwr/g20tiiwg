import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:g20tiiwg/services/apis.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:g20tiiwg/screen/photo_details.dart';
import 'package:g20tiiwg/screen/release_details.dart';

class HomeComponent extends StatefulWidget {
  @override
  HomeComponentState createState() => HomeComponentState();
}

class HomeComponentState extends State<HomeComponent> {
  PageController? pageController;
  int pageIndex = 0;

  @override
  void initState() {
    super.initState();
    final provStage = Provider.of<Apis>(context, listen: false);
    provStage.getBanner().whenComplete(() => {
          provStage.getPhoto(limit: 2).whenComplete(() => {setState(() {})}),
          setState(() {}),
        });
    provStage.getStatistic().whenComplete(() => {setState(() {})});
    provStage.getRelease(limit: 2).whenComplete(() => {setState(() {})});
    init();
  }

  Future<void> init() async {
    pageController =
        PageController(initialPage: pageIndex, viewportFraction: 0.9);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final provApis = Provider.of<Apis>(context, listen: true);
    return SingleChildScrollView(
      child: Column(
        children: [
          16.height,
          (provApis.loadingBanner == false)
              ? Container(
                  height: 200,
                  child: PageView(
                    controller: pageController,
                    children:
                        List.generate(provApis.listBanner.length, (index) {
                      return Image.network(
                        (provApis.listBanner[index]['type'] == 'Photo')
                            ? 'https://www.g20sideevents.id/assets/upload/media/' +
                                DateFormat('yyyy')
                                    .format(DateTime.parse(provApis
                                        .listBanner[index]['createdon']))
                                    .toString() +
                                '/' +
                                provApis.listBanner[index]['news_id'] +
                                '/' +
                                provApis.listBanner[index]['filename']
                            : 'https://i.ytimg.com/vi/' +
                                YoutubePlayer.convertUrlToId(
                                        provApis.listVideo[index]['filename'])
                                    .toString() +
                                '/maxresdefault.jpg',
                        fit: BoxFit.cover,
                        loadingBuilder: (BuildContext context, Widget child,
                            ImageChunkEvent? loadingProgress) {
                          if (loadingProgress == null) return child;
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        },
                      )
                          .cornerRadiusWithClipRRect(16)
                          .paddingRight(pageIndex < 2 ? 8 : 0);
                    }),
                    onPageChanged: (value) {},
                  ),
                )
              : Container(
                  height: 200,
                  child: Center(
                    child: CircularProgressIndicator(),
                  )),
          8.height,
          DotIndicator(
              pageController: pageController!,
              pages: provApis.listBanner,
              indicatorColor: Color(0xFFFD5530)),
          16.height,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('G20 Statistics', style: boldTextStyle(size: 20)),
            ],
          ).paddingOnly(left: 16, right: 16),
          //16.height,
          Container(
            margin: EdgeInsets.only(top: 10),
            child: ResponsiveGridRow(
              children: [
                ResponsiveGridCol(
                  lg: 6,
                  xs: 6,
                  md: 6,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 0.0, left: 0, bottom: 0, right: 5),
                    child: Card(
                      semanticContainer: true,
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      elevation: 3,
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border(
                            top: BorderSide(color: Color(0xff2f62a0), width: 3),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: ResponsiveGridRow(
                            children: [
                              ResponsiveGridCol(
                                xs: 3,
                                md: 3,
                                child: Padding(
                                    padding: const EdgeInsets.only(top: 10.0),
                                    child: InkWell(
                                      onTap: () {},
                                      child: Icon(
                                        Icons.dashboard,
                                        color: Color(0xffff7189),
                                        size: 50.0,
                                      ),
                                    )),
                              ),
                              ResponsiveGridCol(
                                xs: 9,
                                md: 9,
                                child: Container(
                                  alignment: Alignment.topLeft,
                                  child: ListTile(
                                    title: Text(
                                      'TIIWG',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: Color(0xff233048),
                                          fontSize: 14.0),
                                    ),
                                    subtitle: Text(
                                      (provApis.mapStatistic['jml_tiiwg'] !=
                                              null)
                                          ? provApis.mapStatistic['jml_tiiwg']
                                          : '-',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: Color(0xff233048),
                                          fontWeight: FontWeight.w700,
                                          fontSize: 20.0),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                ResponsiveGridCol(
                  lg: 6,
                  xs: 6,
                  md: 6,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 0.0, left: 5, bottom: 0, right: 5),
                    child: Card(
                      semanticContainer: true,
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      elevation: 3,
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border(
                            top: BorderSide(color: Color(0xff2f62a0), width: 3),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: ResponsiveGridRow(
                            children: [
                              ResponsiveGridCol(
                                xs: 3,
                                md: 3,
                                child: Padding(
                                    padding: const EdgeInsets.only(top: 10.0),
                                    child: InkWell(
                                      onTap: () {},
                                      child: Icon(
                                        Icons.event_seat,
                                        color: Color(0xffff7189),
                                        size: 50.0,
                                      ),
                                    )),
                              ),
                              ResponsiveGridCol(
                                xs: 9,
                                md: 9,
                                child: Container(
                                  alignment: Alignment.topLeft,
                                  child: ListTile(
                                    title: Text(
                                      'Road To G20',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: Color(0xff233048),
                                          fontSize: 14.0),
                                    ),
                                    subtitle: Text(
                                      (provApis.mapStatistic[
                                                  'jml_side_event'] !=
                                              null)
                                          ? provApis
                                              .mapStatistic['jml_side_event']
                                          : '-',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: Color(0xff233048),
                                          fontWeight: FontWeight.w700,
                                          fontSize: 20.0),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                ResponsiveGridCol(
                  lg: 6,
                  xs: 6,
                  md: 6,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, left: 0, bottom: 0, right: 5),
                    child: Card(
                      semanticContainer: true,
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      elevation: 3,
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border(
                            top: BorderSide(color: Color(0xff2f62a0), width: 3),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: ResponsiveGridRow(
                            children: [
                              ResponsiveGridCol(
                                xs: 3,
                                md: 3,
                                child: Padding(
                                    padding: const EdgeInsets.only(top: 10.0),
                                    child: InkWell(
                                      onTap: () {},
                                      child: Icon(
                                        Icons.date_range,
                                        color: Color(0xffff7189),
                                        size: 50.0,
                                      ),
                                    )),
                              ),
                              ResponsiveGridCol(
                                xs: 9,
                                md: 9,
                                child: Container(
                                  alignment: Alignment.topLeft,
                                  child: ListTile(
                                    title: Text(
                                      'Side Events',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: Color(0xff233048),
                                          fontSize: 14.0),
                                    ),
                                    subtitle: Text(
                                      (provApis.mapStatistic[
                                                  'jml_road_to_g20'] !=
                                              null)
                                          ? provApis
                                              .mapStatistic['jml_road_to_g20']
                                          : '-',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: Color(0xff233048),
                                          fontWeight: FontWeight.w700,
                                          fontSize: 20.0),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                ResponsiveGridCol(
                  lg: 6,
                  xs: 6,
                  md: 6,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, left: 5, bottom: 0, right: 5),
                    child: Card(
                      semanticContainer: true,
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      elevation: 3,
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border(
                            top: BorderSide(color: Color(0xff2f62a0), width: 3),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: ResponsiveGridRow(
                            children: [
                              ResponsiveGridCol(
                                xs: 3,
                                md: 3,
                                child: Padding(
                                    padding: const EdgeInsets.only(top: 10.0),
                                    child: InkWell(
                                      onTap: () {},
                                      child: Icon(
                                        Icons.library_books,
                                        color: Color(0xffff7189),
                                        size: 50.0,
                                      ),
                                    )),
                              ),
                              ResponsiveGridCol(
                                xs: 9,
                                md: 9,
                                child: Container(
                                  alignment: Alignment.topLeft,
                                  child: ListTile(
                                    title: Text(
                                      'Total Events',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: Color(0xff233048),
                                          fontSize: 14.0),
                                    ),
                                    subtitle: Text(
                                      (provApis.mapStatistic['jml_event'] !=
                                              null)
                                          ? provApis.mapStatistic['jml_event']
                                          : '-',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: Color(0xff233048),
                                          fontWeight: FontWeight.w700,
                                          fontSize: 20.0),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ).paddingOnly(left: 10, right: 10),
          32.height,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Latest News', style: boldTextStyle(size: 20)),
              //Text('Show More', style: boldTextStyle(color: Color(0xFFFD5530))).onTap(() {}),
            ],
          ).paddingOnly(left: 16, right: 16),
          (provApis.loadingPhoto == false)
              ? ListView.separated(
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  itemCount: provApis.listPhoto.length,
                  padding: EdgeInsets.all(16),
                  itemBuilder: (context, index) {
                    return Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Photo News',
                                style: boldTextStyle(color: Color(0xFFFD5530))),
                            Text(provApis.listPhoto[index]['title_en'],
                                style: boldTextStyle(),
                                softWrap: true,
                                maxLines: 3),
                            8.height,
                            Text(
                                DateFormat('EEEE, d MMMM yyyy')
                                    .format(DateTime.parse(
                                        provApis.listPhoto[index]['createdon']))
                                    .toString(),
                                style: secondaryTextStyle()),
                          ],
                        ).expand(flex: 2),
                        4.width,
                        Image.network(
                          'https://www.g20sideevents.id/assets/upload/media/' +
                              DateFormat('yyyy')
                                  .format(DateTime.parse(
                                      provApis.listPhoto[index]['createdon']))
                                  .toString() +
                              '/' +
                              provApis.listPhoto[index]['news_id'] +
                              '/' +
                              provApis.listPhoto[index]['filename'],
                          height: 100,
                          fit: BoxFit.fill,
                          loadingBuilder: (BuildContext context, Widget child,
                              ImageChunkEvent? loadingProgress) {
                            if (loadingProgress == null) return child;
                            return Center(
                              child: CircularProgressIndicator(
                                value: loadingProgress.expectedTotalBytes !=
                                        null
                                    ? loadingProgress.cumulativeBytesLoaded /
                                        loadingProgress.expectedTotalBytes!
                                    : null,
                              ),
                            );
                          },
                        ).cornerRadiusWithClipRRect(16).expand(flex: 1),
                      ],
                    ).onTap(() {
                      PhotoDetailsScreen(dataDetail: provApis.listPhoto[index])
                          .launch(context);
                    });
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                )
              : Center(
                  child: CircularProgressIndicator(),
                ),
          32.height,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Latest Press Release', style: boldTextStyle(size: 20)),
              //Text('Show More', style: boldTextStyle(color: Color(0xFFFD5530))).onTap(() {}),
            ],
          ).paddingOnly(left: 16, right: 16),
          (provApis.loadingRelease == false)
              ? ListView.separated(
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  itemCount: provApis.listRelease.length,
                  padding: EdgeInsets.all(16),
                  itemBuilder: (context, index) {
                    return Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Press Release',
                                style: boldTextStyle(color: Color(0xFFFD5530))),
                            Text(provApis.listRelease[index]['title_en'],
                                style: boldTextStyle(),
                                softWrap: true,
                                maxLines: 3),
                            8.height,
                            Text(
                                DateFormat('EEEE, d MMMM yyyy')
                                    .format(DateTime.parse(provApis
                                        .listRelease[index]['createdon']))
                                    .toString(),
                                style: secondaryTextStyle()),
                          ],
                        ).expand(flex: 2),
                      ],
                    ).onTap(() {
                      ReleaseDetailsScreen(
                              dataDetail: provApis.listRelease[index])
                          .launch(context);
                    });
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                )
              : Center(
                  child: CircularProgressIndicator(),
                ),
        ],
      ),
    );
  }
}
