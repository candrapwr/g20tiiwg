import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:g20tiiwg/screen/photo_details.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:g20tiiwg/services/apis.dart';

class PhotoComponent extends StatefulWidget {
  @override
  PhotoComponentState createState() => PhotoComponentState();
}

class PhotoComponentState extends State<PhotoComponent> {
  @override
  void initState() {
    super.initState();
    Provider.of<Apis>(context, listen: false)
        .getPhoto()
        .whenComplete(() => {setState(() {})});
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    final provApis = Provider.of<Apis>(context, listen: true);
    return (provApis.loadingPhoto == false)
        ? ListView.separated(
            shrinkWrap: true,
            physics: ScrollPhysics(),
            itemCount: provApis.listPhoto.length,
            padding: EdgeInsets.all(16),
            itemBuilder: (context, index) {
              return Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Photo News',
                          style: boldTextStyle(color: Color(0xFFFD5530))),
                      Text(provApis.listPhoto[index]['title_en'],
                          style: boldTextStyle(), softWrap: true, maxLines: 3),
                      8.height,
                      Text(
                          DateFormat('EEEE, d MMMM yyyy')
                              .format(DateTime.parse(
                                  provApis.listPhoto[index]['createdon']))
                              .toString(),
                          style: secondaryTextStyle()),
                    ],
                  ).expand(flex: 2),
                  4.width,
                  Image.network(
                    'https://www.g20sideevents.id/assets/upload/media/' +
                        DateFormat('yyyy')
                            .format(DateTime.parse(
                                provApis.listPhoto[index]['createdon']))
                            .toString() +
                        '/' +
                        provApis.listPhoto[index]['news_id'] +
                        '/' +
                        provApis.listPhoto[index]['filename'],
                    height: 100,
                    fit: BoxFit.fill,
                    loadingBuilder: (BuildContext context, Widget child,
                        ImageChunkEvent? loadingProgress) {
                      if (loadingProgress == null) return child;
                      return Center(
                        child: CircularProgressIndicator(
                          value: loadingProgress.expectedTotalBytes != null
                              ? loadingProgress.cumulativeBytesLoaded /
                                  loadingProgress.expectedTotalBytes!
                              : null,
                        ),
                      );
                    },
                  ).cornerRadiusWithClipRRect(16).expand(flex: 1),
                ],
              ).onTap(() {
                PhotoDetailsScreen(dataDetail: provApis.listPhoto[index])
                    .launch(context);
              });
            },
            separatorBuilder: (context, index) {
              return Divider();
            },
          )
        : Center(
            child: CircularProgressIndicator(),
          );
  }
}
